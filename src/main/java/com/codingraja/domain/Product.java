package com.codingraja.domain;

import java.util.Set;

public class Product {
	private Long id;
	private String name;
	private String model;
	private String brand;
	private Double price;
	private Set<String> colors;
	
	public Product(){}
	public Product(String name, String model, String brand, Double price, Set<String> colors) {
		super();
		this.name = name;
		this.model = model;
		this.brand = brand;
		this.price = price;
		this.colors = colors;
	}
	
	public Long getId() {
		return id;
	}
	public void setId(Long id) {
		this.id = id;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public String getModel() {
		return model;
	}
	public void setModel(String model) {
		this.model = model;
	}
	public String getBrand() {
		return brand;
	}
	public void setBrand(String brand) {
		this.brand = brand;
	}
	public Double getPrice() {
		return price;
	}
	public void setPrice(Double price) {
		this.price = price;
	}
	public Set<String> getColors() {
		return colors;
	}
	public void setColors(Set<String> colors) {
		this.colors = colors;
	}
}
