package com.codingraja.test;

import java.util.Set;
import java.util.TreeSet;

import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.Transaction;
import org.hibernate.cfg.Configuration;

import com.codingraja.domain.Product;

public class SaveCustomer {

	public static void main(String[] args) {
		
		Configuration configuration = new Configuration();
		configuration.configure("hibernate.cfg.xml");
		
		SessionFactory factory = configuration.buildSessionFactory();
		
		Set<String> colors = new TreeSet<String>();
		colors.add("Gray");
		colors.add("Blue");
		colors.add("White");
		
		Product product = new Product("MacBook", "MacBook Air", "Apple", 75000.0, colors);
		
		Session session = factory.openSession();
		Transaction transaction = session.beginTransaction();
		session.save(product);
		transaction.commit();
		session.close();
		
		System.out.println("Product has been saved successfully!");
	}

}
